// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas.Implementation;

import katas.OtherTeamSettingsLibrary.RenderSettings;
import katas.ThirdPartyLibrary.GridView;
import katas.UIFramework.Control;

public final class CustomGrid extends GridView {
    private StyleRule[] customFormattingRules;

    public StyleRule[] getCustomFormattingRules() {
        return customFormattingRules;
    }

    public void setCustomFormattingRules(StyleRule[] value) {
        customFormattingRules = value;
    }

    @Override
    protected void onUpdateCellStyle(Cell cell) {
        super.onUpdateCellStyle(cell);

        if (!RenderSettings.getIsVisionImpaired()) {
            cell.getControl().setBorderType(BorderType.Dotted);
            cell.getControl().setBorderWeight(0.5);
        } else {
            cell.getControl().setBorderType(Control.BorderType.Double);
            cell.getControl().setBorderWeight(2);
        }
        cell.getControl().setBorderCollapse(true);
    }

    public static class StyleRule {
        private CellCondition condition;

        public CellCondition getCondition() {
            return condition;
        }

        private StyleMod styleMod;

        public StyleMod getStyleMod() {
            return styleMod;
        }

        public StyleRule(CellCondition condition, StyleMod styleMod) {
            this.condition = condition;
            this.styleMod = styleMod;
        }
    }
}


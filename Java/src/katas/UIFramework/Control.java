// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas.UIFramework;

public abstract class Control {
    public enum BorderType {
        None,
        Dashed,
        Dotted,
        Solid,
        Double,
    }

    public static class Color {
        public Color(double r, double g, double b, double a) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        private double r;

        public double getR() {
            return r;
        }

        private double g;

        public double getG() {
            return g;
        }

        private double b;

        public double getB() {
            return b;
        }

        private double a;

        public double getA() {
            return a;
        }
    }

    static {
        UI.requireUIThread();
    }

    protected Control() {
        UI.requireUIThread();
    }

    public abstract void updateStyle();

    public final void setBorderType(BorderType borderType) {
        UI.requireUIThread();
    }

    public final void setBorderWeight(double weight) {
        UI.requireUIThread();
    }

    public final void setBackgroundColor(Color color) {
        UI.requireUIThread();
    }

    public final void setTextColor(Color color) {
        UI.requireUIThread();
    }

    public final void setTextWeight(double weight) {
        UI.requireUIThread();
    }

    public final void setTextUnderline(boolean isUnderlined) {
        UI.requireUIThread();
    }

    public final void setTextItalic(boolean isItalic) {
        UI.requireUIThread();
    }

    public final void setBorderCollapse(boolean collapse) {
        UI.requireUIThread();
    }
}


// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas.ThirdPartyLibrary;

import katas.UIFramework.Control;
import katas.UIFramework.UI;

import java.util.*;

public abstract class GridView extends Control {
    public interface Row {
        int getPosition();

        Cell[] getCells();

        Cell getCell(String name);

        Cell getCell(int ordinalPosition);

        Cell getCell(Column column);
    }

    private class RowImpl implements Row {
        private final Map<Column, Cell> cells = new HashMap<>();
        private final GridView parent;

        RowImpl(GridView parent) {
            this.parent = parent;

            for (Column parentColumn : this.parent.columns)
                cells.put(parentColumn, new CellImpl(parent, this, parentColumn));
        }

        public int getPosition() {
            return parent.rows.indexOf(this);
        }

        public Cell[] getCells() {
            ArrayList<Cell> result = new ArrayList<>();
            for (Column column : parent.columns)
                result.add(getCell(column));

            return result.toArray(new Cell[0]);
        }

        public Cell getCell(String name) {
            Column column = parent.getColumn(name);

            return getCell(column);
        }

        public Cell getCell(int ordinalPosition) {
            return getCell(parent.columns.get(ordinalPosition));
        }

        public Cell getCell(Column column) {
            return cells.get(column);
        }
    }

    public interface Column {
        String getName();

        int getPosition();

        Cell[] getCells();

        Cell getCellByRowIndex(int index);

        Cell getCell(Row row);
    }

    private class ColumnImpl implements Column {
        private final String name;
        private final GridView parent;

        ColumnImpl(String name, GridView parent) {
            this.name = name;
            this.parent = parent;
        }

        public String getName() {
            return name;
        }

        public int getPosition() {
            return parent.columns.indexOf(this);
        }

        public Cell[] getCells() {
            ArrayList<Cell> result = new ArrayList<>();

            for (Row row : parent.rows)
                result.add(row.getCell(this));

            return result.toArray(new Cell[0]);
        }

        public Cell getCellByRowIndex(int index) {
            return parent.rows.get(index).getCell(this);
        }

        public Cell getCell(Row row) {
            return row.getCell(this);
        }
    }

    public interface Cell {
        Object value();

        void value(Object value);

        Column getColumn();

        Row getRow();

        Control getControl();
    }

    private static class CellImpl implements Cell {
        private class CellControl extends Control {
            private final CellImpl cell;
            private final GridView parent;

            public CellControl(CellImpl cell, GridView parent) {
                this.cell = cell;
                this.parent = parent;
            }

            @Override
            public void updateStyle() {
                UI.requireUIThread();
                parent.onUpdateCellStyle(cell);
            }
        }

        private final Column column;
        private final Control control;
        private final Row row;

        CellImpl(GridView gridView, Row row, Column column) {
            this.row = row;
            this.column = column;
            control = new CellControl(this, gridView);
        }

        public Column getColumn() {
            return column;
        }

        public Row getRow() {
            return row;
        }

        private Object _Value;

        public Object value() {
            return _Value;
        }

        public void value(Object value) {
            _Value = value;
        }

        public Control getControl() {
            return control;
        }
    }

    private final List<Column> columns = new ArrayList<>();
    private final List<Row> rows = new ArrayList<>();

    public Iterable<Cell> selectCells(CellCondition condition) {
        ArrayList<Cell> result = new ArrayList<>();
        for (Row row : rows)
            for (Cell cell : row.getCells())
                if (condition.appliesTo(cell))
                    result.add(cell);

        return result;
    }

    public interface CellCondition {
        boolean appliesTo(Cell cell);
    }

    public Column getColumn(String name) {
        for (Column column : columns)
            if (column.getName() == name)
                return column;

        return null;
    }

    public void setHeadings(String[] names) {
        if (rows.size() > 0)
            throw new IllegalStateException();

        columns.clear();
        for (String name : names)
            columns.add(new ColumnImpl(name, this));
    }

    public Row newRow() {
        RowImpl result = new RowImpl(this);
        rows.add(result);
        return result;
    }

    public void deleteRow(Row toDelete) {
        rows.remove(toDelete);
    }

    @Override
    public final void updateStyle() {
        UI.requireUIThread();
    }

    protected void onUpdateCellStyle(Cell cell) {
    }
}


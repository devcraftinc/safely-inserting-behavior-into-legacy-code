﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UIFramework;

namespace Implementation
{
  public abstract class Control
  {
    public enum BorderType
    {
      None,
      Dashed,
      Dotted,
      Solid,
      Double,
    }

    public class Color
    {
      public Color(double r, double g, double b, double a)
      {
        R = r;
        G = g;
        B = b;
        A = a;
      }

      public double R { get; }
      public double G { get; }
      public double B { get; }
      public double A { get; }
    }

    static Control()
    {
      UI.RequireUIThread();
    }

    protected Control()
    {
      UI.RequireUIThread();
    }

    public abstract void UpdateStyle();

    // final
    public void SetBorderType(BorderType borderType)
    {
      UI.RequireUIThread();
    }

    // final
    public void SetBorderWeight(double weight)
    {
      UI.RequireUIThread();
    }

    // final
    public void SetBackgroundColor(Color color)
    {
      UI.RequireUIThread();
    }

    // final
    public void SetTextColor(Color color)
    {
      UI.RequireUIThread();
    }

    // final
    public void SetTextWeight(double weight)
    {
      UI.RequireUIThread();
    }

    // final
    public void SetTextUnderline(bool isUnderlined)
    {
      UI.RequireUIThread();
    }

    // final
    public void SetTextItalic(bool isItalic)
    {
      UI.RequireUIThread();
    }

    // final
    public void SetBorderCollapse(bool collapse)
    {
      UI.RequireUIThread();
    }
  }
}
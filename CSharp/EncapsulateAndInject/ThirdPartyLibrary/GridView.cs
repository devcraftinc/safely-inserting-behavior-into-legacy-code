﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using UIFramework;

namespace Implementation
{
  public abstract class GridView : Control
  {
    public interface Row
    {
      int GetPosition();
      Cell[] GetCells();
      Cell GetCell(string name);
      Cell GetCell(int ordinalPosition);
      Cell GetCell(Column column);
    }

    private class RowImpl : Row
    {
      private readonly IDictionary<Column, Cell> cells = new Dictionary<Column, Cell>();
      private readonly GridView parent;

      public RowImpl(GridView parent)
      {
        this.parent = parent;

        foreach (var parentColumn in this.parent.columns)
          cells.Add(parentColumn, new CellImpl(parent, this, parentColumn));
      }

      public int GetPosition()
      {
        return parent.rows.IndexOf(this);
      }

      public Cell[] GetCells()
      {
        var result = new List<Cell>();
        foreach (var column in parent.columns)
          result.Add(GetCell(column));

        return result.ToArray();
      }

      public Cell GetCell(string name)
      {
        var column = parent.GetColumn(name);

        return GetCell(column);
      }

      public Cell GetCell(int ordinalPosition)
      {
        return GetCell(parent.columns[ordinalPosition]);
      }

      public Cell GetCell(Column column)
      {
        return cells[column];
      }
    }

    public interface Column
    {
      string GetName();
      int GetPosition();
      Cell[] GetCells();
      Cell GetCellByRowIndex(int index);
      Cell GetCell(Row row);
    }

    private class ColumnImpl : Column
    {
      private readonly string name;
      private readonly GridView parent;

      public ColumnImpl(string name, GridView parent)
      {
        this.name = name;
        this.parent = parent;
      }

      public string GetName()
      {
        return name;
      }

      public int GetPosition()
      {
        return parent.columns.IndexOf(this);
      }

      public Cell[] GetCells()
      {
        var result = new List<Cell>();

        foreach (var row in parent.rows)
          result.Add(row.GetCell(this));

        return result.ToArray();
      }

      public Cell GetCellByRowIndex(int index)
      {
        return parent.rows[index].GetCell(this);
      }

      public Cell GetCell(Row row)
      {
        return row.GetCell(this);
      }
    }

    public interface Cell
    {
      object Value { get; set; }
      Column GetColumn();
      Row GetRow();
      Control GetControl();
    }

    private class CellImpl : Cell
    {
      private class CellControl : Control
      {
        private readonly CellImpl cell;
        private readonly GridView parent;

        public CellControl(CellImpl cell, GridView parent)
        {
          this.cell = cell;
          this.parent = parent;
        }

        public override void UpdateStyle()
        {
          UI.RequireUIThread();
          parent.OnUpdateCellStyle(cell);
        }
      }

      private readonly Column column;
      private readonly Control control;
      private readonly Row row;

      public CellImpl(GridView gridView, Row row, Column column)
      {
        this.row = row;
        this.column = column;
        control = new CellControl(this, gridView);
      }

      public Column GetColumn()
      {
        return column;
      }

      public Row GetRow()
      {
        return row;
      }

      public object Value { get; set; }

      public Control GetControl()
      {
        return control;
      }
    }

    public interface CellCondition
    {
      bool AppliesTo(Cell cell);
    }

    private readonly IList<Column> columns = new List<Column>();
    private readonly IList<Row> rows = new List<Row>();

    public IEnumerable<Cell> SelectCells(CellCondition condition)
    {
      var result = new List<Cell>();
      foreach (var row in rows)
      foreach (var cell in row.GetCells())
        if (condition.AppliesTo(cell))
          result.Add(cell);

      return result;
    }

    public Column GetColumn(string name)
    {
      foreach (var column in columns)
        if (column.GetName() == name)
          return column;

      return null;
    }

    public void SetHeadings(string[] names)
    {
      if (rows.Count > 0)
        throw new InvalidOperationException();

      columns.Clear();
      foreach (var name in names)
        columns.Add(new ColumnImpl(name, this));
    }

    public Row NewRow()
    {
      var result = new RowImpl(this);
      rows.Add(result);
      return result;
    }

    public void DeleteRow(Row toDelete)
    {
      rows.Remove(toDelete);
    }

    public sealed override void UpdateStyle()
    {
      UI.RequireUIThread();
    }

    protected virtual void OnUpdateCellStyle(Cell cell)
    {
    }
  }
}